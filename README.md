Developer Notes:

The developer used the best practices and approaches in Creating RESTFul Applications
This project was made using:

- Python 3.6
- Django Standard Project Structure (created by the Developer)
- Django REST Auth

API Endpoints:

NAME: Register Endpoint
URL: localhost:8000/auth/registration/
METHOD: POST
DATA:
{
    "password1":"adminone",
    "password2":"adminone",
    "email":"adminone@admin.com",
    "first_name": "Ray Marc",
    "last_name": "Marcellones"
}

NAME: Login Endpoint
URL: localhost:8000/auth/login/
METHOD: POST
DATA:
{
    "email":"adminone@admin.com",
    "password":"adminone",
}

NAME: Users List
URL: localhost:8000/api/v1/users/
HEADERS: Authorization: Token <TOKEN_HERE>
METHOD: GET

NAME: Change Password
URL: localhost:8000/auth/password/change/
HEADERS: Authorization: Token <TOKEN_HERE>
METHOD: POST
DATA:
{
    "old_password": "oldpassword",
    "new_password1": "newpassword",
    "new_password2": "newpassword"
}
